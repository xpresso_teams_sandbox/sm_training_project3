"""
This is a sample hello world app
It prints hello world 100 times
"""
__author__ = "Naveen Sinha"

import time
import logging
from xpresso_ai.xpresso.ai.core.logging.xpr_log import XprLogger

if __name__ == '__main__':
  logger = XprLogger(level=logging.DEBUG)
  COUNT_TIMES = 10
  while COUNT_TIMES:
    print(COUNT_TIMES)
    logger.debug("count is {}".format(str(COUNT_TIMES)))
    COUNT_TIMES-=1
    time.sleep(1)
